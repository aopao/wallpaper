<!doctype html>
<html lang="zh-CN">
<head>
    @include('layouts.header')
</head>
<body>
    <header>
        @include('layouts.nav')
    </header>
    <section class="picture-list">
        @include('layouts.list')
    </section>
<!-- Optional JavaScript -->
@include('layouts.script')
</body>
</html>
