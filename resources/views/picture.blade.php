<!doctype html>
<html lang="zh-CN">
<head>
    @include('layouts.header')
</head>
<body>
<div class="picture-wrapper">
    <div class="operation">
        <div class="float-left">
            <button class="btn btn-light" id="navigation-back">
                <i class="fa fa-chevron-left"></i>
                <span>返回</span>
            </button>
        </div>
        <div class="float-right">
            <button class="btn btn-light picture-download" data-id="{{ $item['id'] }}">
                <i class="fa fa-download"></i>
                <span>下载</span>
            </button>
        </div>
    </div>
    <img class="animate__animated animate__flipInX" src="{{ $item['outside_url'] }}" alt="">
    <div class="description">
        <p class="title">{{ $item['title'] }}</p>
        <p class="desc">{{ $item['description'] }}</p>
        <p class="calendari icon icon-calendar">
            <i class="fa fa-calendar"></i>
            <em>2020-11-11</em>
        </p>
    </div>
</div>
<!-- Optional JavaScript -->
@include('layouts.script')
</body>
</html>
