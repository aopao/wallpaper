
<div class="pictures container-fluid">
    <div id="masonry" class="row">
        @foreach($items as $item)
        <div class="picture-item col-md-3" data-id="{{ $item['id'] }}" data-website="{{ $item['website_en'] }}">
            <div class="picture-detail">
                <p class="title">{{ $item['title'] }}</p>
                <p>
                    <i class="fa fa-calendar"></i>
                    <em>{{ $item['remark'] }}</em>
                </p>
                {{--<p>--}}
                {{--    <i class="fa fa-eye"></i>--}}
                {{--    <em>{{ $item['view'] }}</em>--}}
                {{--</p>--}}
            </div>
            <img src="{{ $item['outside_url'] }}" alt="">
            <div class="picture-operation">
                <span>
                    <i class="fa fa-download"></i>
                </span>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div>
    {{ $items->links() }}
</div>
