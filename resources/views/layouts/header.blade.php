<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ URL::asset('frameworks/bootstrap/css/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ URL::asset('frameworks/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('frameworks/animate.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('dist/css/style.css') }}">

<title>壁纸库</title>
