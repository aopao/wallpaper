$(() => {
    $('#masonry').masonry({
        itemSelector: '.picture-item',
        isAnimated: true,
    })
    $('#masonry').imagesLoaded().progress(() => {
        $('#masonry').masonry('layout')
    })
})

$(document).on('click', '.picture-item', event => {
    let id = $(event.currentTarget).data('id')
    location.href = `/picture/${id}`
})

$(document).on('click', '#navigation-back', event => {
    history.back()
})

$(document).on('click', '.picture-download', event => {
    let id = $(event.currentTarget).data('id')
    downloadPicture(id)
})

function downloadPicture(id) {
    $.ajax({
        url: `/api/picture/${id}/download`,
        method: 'post',
        success: res => {
            let data = res.data.replace('http://', 'https://')
            download(data);
        }
    })
}
