<?php

use App\Helpers\Bing;
use App\Http\Controllers\BingController;
use App\Http\Controllers\PictureController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('test', function () {
    $b = new Bing();
    return $b->getLatestPicCN();
    // return downloadBing();
});

Route::prefix('picture')->group(function () {
    Route::post('{id}/download', [PictureController::class, 'download']);
});
