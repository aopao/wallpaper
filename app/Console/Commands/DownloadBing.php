<?php

namespace App\Console\Commands;

use App\Helpers\Bing;
use Illuminate\Console\Command;

class DownloadBing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:bing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '定时下载图片';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        downloadBing();
        return 0;
    }

}
