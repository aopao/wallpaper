<?php

namespace App\Http\Controllers;

use App\Models\Picture;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $items = Picture::orderBy('created_at', 'desc')->paginate(16);
        return view('home', compact('items'));
    }

    public function detail(Request $request, $id)
    {
        $item = Picture::find($id);
        $item['view'] += 1;
        $item->save();
        return view('picture', compact('item'));
    }
}
