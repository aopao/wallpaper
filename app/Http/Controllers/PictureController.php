<?php

namespace App\Http\Controllers;

use App\Models\Picture;
use Illuminate\Http\Request;

class PictureController extends Controller
{
    public function download(Request $request, $id)
    {
        $item = Picture::find($id);
        $item['down'] += 1;
        $item->save();
        return rsp($item['outside_url']);
    }
}
